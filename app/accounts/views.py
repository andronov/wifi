import hashlib
import json
import time
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.shortcuts import redirect
from accounts import UserProfile
import random
from core.models import Account, Entered, PointWifi


def _createHash():
    """
    Function for creating 6 symbols random hash for referral code
    """
    rad = random.sample(range(1,10),  4)
    return ''.join(str(x) for x in rad)

def _createUsername():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.sha1()
    hash.update(str(time.time()))
    return hash.hexdigest()[:25]

def create_account(request):
    data = {}
    phone = request.POST['phone']
    if phone:
        if phone[:1] == '7':
            phone = phone[1:]
        email = str(_createUsername()) + '@mail.ru'
        passw =_createHash()
        usr = UserProfile.objects.filter(phone=phone)
        if usr:
            user = usr[0]
            user.set_password(passw)
            user.first_name = passw
            user.save()
            data['new'] = True
        else:
            user = UserProfile.objects.create_user(username=_createUsername(), email=email, password=passw)
            user.first_name = passw
            user.phone = phone
            user.set_password(passw)
            user.save()
            wifi = PointWifi.objects.get(id=3)
            acc = Account.objects.create(user=wifi.user, phone=user.phone)
        from nexmo.libpynexmo.nexmomessage import NexmoMessage
        params = {
                'api_key': settings.NEXMO_USERNAME,
                'api_secret': settings.NEXMO_PASSWORD,
                'type': 'unicode',
                'from': 'wifi',
                'to': '+7' +str(phone),
                'text': passw,
        }
        sms = NexmoMessage(params)
        response = sms.send_request()

        data['success'] = True
        data['errors'] = False
        return HttpResponse(json.dumps(data), content_type="application/json")


def my_login(request):
    data = {}
    phone = request.POST['phone']
    password = request.POST['code']
    users = UserProfile.objects.filter(phone=phone)
    acc = Account.objects.get(phone=users[0].phone)
    wifi = PointWifi.objects.get(id=3)
    if users:
        user = authenticate(username=users[0].username, password=password)
        if user is not None:
            enter = Entered.objects.create(user=wifi.user,wifi=wifi, account=acc)
            data['errors'] = True
            #login(request, user)

    else:
        data['errors'] = False
    return HttpResponse(json.dumps(data), content_type="application/json")