from django.conf import settings
from core.models import *


def send(id):
    sub = Subscriptions.objects.get(id=id)
    accounts = Account.objects.filter(user=sub.account, is_active=True)
    wifi = PointWifi.objects.filter(user=sub.account)
    for acc in accounts:
      for wi in wifi:
        from nexmo.libpynexmo.nexmomessage import NexmoMessage
        params = {
                'api_key': settings.NEXMO_USERNAME,
                'api_secret': settings.NEXMO_PASSWORD,
                'type': 'unicode',
                'from': '7' +str(sub.phone),
                'to': '+7' +str(acc.phone),
                'text': sub.content.encode('utf-8'),
        }
        sms = NexmoMessage(params)
        response = sms.send_request()

        obj = Messages.objects.create(base_account=sub.account, wifi=wi, account=acc, admin=True,text=sub.content,
                                           status=response['messages'][0]['status'])

        if int(response['messages'][0]['status']) == 0:
            ms = Messages.objects.get(id=obj.id)
            ms.id_ms = response['messages'][0]['message-id']
            ms.save()

def send_condition(id, accs):
    sub = Subscriptions.objects.get(id=id)
    accounts = Account.objects.filter(id__in=accs)

    for acc in accounts:
        enter = Entered.objects.filter(account=acc).order_by('-created')[0]
        from nexmo.libpynexmo.nexmomessage import NexmoMessage
        params = {
                'api_key': settings.NEXMO_USERNAME,
                'api_secret': settings.NEXMO_PASSWORD,
                'type': 'unicode',
                'from': '7' +str(sub.phone),
                'to': '+7' +str(acc.phone),
                'text': sub.content.encode('utf-8'),
        }
        sms = NexmoMessage(params)
        response = sms.send_request()

        obj = Messages.objects.create(base_account=sub.account, wifi=enter.wifi, account=acc, admin=True,text=sub.content,
                                           status=response['messages'][0]['status'])

        if int(response['messages'][0]['status']) == 0:
            ms = Messages.objects.get(id=obj.id)
            ms.id_ms = response['messages'][0]['message-id']
            ms.save()
    if sub.run_sub == 2:
        sub.send = True
        sub.save()
