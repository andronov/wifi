# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_account_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='entered',
            name='wifi',
            field=models.ForeignKey(related_name='wifi_entered', to='core.PointWifi', null=True),
        ),
    ]
