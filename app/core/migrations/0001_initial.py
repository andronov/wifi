# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('phone', models.CharField(max_length=15, verbose_name=b'phone')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Entered',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('account', models.ForeignKey(related_name='entered_messages', to='core.Account', null=True)),
                ('user', models.ForeignKey(related_name='base_entered_messages', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Messages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('text', models.CharField(max_length=560, verbose_name=b'Text', blank=True)),
                ('admin', models.BooleanField(default=False)),
                ('account', models.ForeignKey(related_name='account_messages', to='core.Account', null=True)),
                ('base_account', models.ForeignKey(related_name='user_messages', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PointWifi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'name')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Subscriptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f \xd1\x88\xd0\xb0\xd0\xb1\xd0\xbb\xd0\xbe\xd0\xbd\xd0\xb0')),
                ('content', models.CharField(max_length=560, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82 \xd1\x81\xd0\xbe\xd0\xbe\xd0\xb1\xd1\x89\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('phone', models.IntegerField(verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xb4\xd0\xbf\xd0\xb8\xd1\x81\xd1\x8c \xd0\xbe\xd1\x82\xd0\xbf\xd1\x80\xd0\xb0\xd0\xb2\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8f')),
                ('run_sub', models.IntegerField(default=1, verbose_name=b'\xd0\x92\xd1\x8b\xd0\xbf\xd0\xbe\xd0\xbb\xd0\xbd\xd1\x8f\xd1\x82\xd1\x8c \xd1\x80\xd0\xb0\xd1\x81\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd1\x83 \xd0\xbf\xd0\xb5\xd1\x80\xd0\xb8\xd0\xbe\xd0\xb4\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd0\xba\xd0\xb8 \xd0\xbf\xd0\xbe \xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c\xd1\x82\xd1\x80\xd1\x83', choices=[(1, b'\xd0\x9d\xd0\xb5 \xd1\x80\xd0\xb0\xd1\x81\xd1\x81\xd1\x8b\xd0\xbb\xd0\xb0\xd1\x82\xd1\x8c'), (2, b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xbe\xd1\x81\xd0\xbb\xd0\xb0\xd1\x82\xd1\x8c 1 \xd1\x80\xd0\xb0\xd0\xb7'), (3, b'\xd0\xa0\xd0\xb0\xd1\x81\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd0\xb0 \xd0\xbf\xd0\xbe \xd1\x83\xd1\x81\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xb8\xd1\x8f\xd0\xbc')])),
                ('now', models.BooleanField(default=False, verbose_name=b'\xd0\xa1\xd0\xb5\xd0\xb9\xd1\x87\xd0\xb0\xd1\x81')),
                ('start_at', models.DateTimeField(null=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbd\xd0\xb0\xd1\x87\xd0\xb0\xd0\xbb\xd0\xb0 \xd1\x80\xd0\xb0\xd1\x81\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd0\xb8', blank=True)),
                ('account', models.ForeignKey(related_name='user_subs', to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='messages',
            name='wifi',
            field=models.ForeignKey(related_name='wifi_messages', to='core.PointWifi', null=True),
        ),
    ]
