# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0003_entered_wifi'),
    ]

    operations = [
        migrations.AddField(
            model_name='pointwifi',
            name='user',
            field=models.ForeignKey(related_name='user_b_wifi', to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
