# -*- coding: utf-8 -*-
from celery.task import task
from django.conf import settings
from core.utils import calc


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def just_print():
    calc()

    tmp_filename = settings.TMP_PATH + '/ms.txt'
    with open(tmp_filename, "wb") as f:
           f.write('gjhgfhj')
    f.close()
    print "Print from celery task"