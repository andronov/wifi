# -*- coding: utf-8 -*-
import json
import datetime
from django.conf import settings
from django.core import serializers
from django.db.models import Count, Q
from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response, redirect

# Create your views here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView
from django.contrib import messages
from nexmo.error_messages import (NEXMO_STATUSES, UNKNOWN_STATUS,
                             NEXMO_MESSAGES, UNKNOWN_MESSAGE)
from rest_framework.generics import RetrieveAPIView
from core.forms import SubForm
from core.models import *
from core.send import send, send_condition
from core.serializers import MessagesSerializer, PointWifiSerializer, SubSerializer
from dateutil.relativedelta import relativedelta

def calc():
    subs = Subscriptions.objects.filter(send=False, created__lte=datetime.datetime.now())
    for sub in subs:
      sub_groups = ConditionGroups.objects.get(subscription=sub)
      print(sub_groups.count.all())

      order = {}
      #count
      for cnt in sub_groups.count.all():
        order[cnt.location] = cnt

      #point
      for cnt in sub_groups.point.all():
        order[cnt.location] = cnt

      #time
      for cnt in sub_groups.time.all():
        order[cnt.location] = cnt

      accs = []
      choice = 1
      for ord in order:
        res = order[ord]
        if hasattr(res, 'plusminus'):
            dates = None
            if res.plusminus == 1:
                if res.dates == 1:
                   dates = res.start_at + relativedelta(days=res.count_time)
                elif res.dates == 2:
                    dates = res.start_at + relativedelta(months=res.count_time)
                elif res.dates == 3:
                    dates = res.start_at + relativedelta(years=res.count_time)
                elif res.dates == 4:
                    dates = res.start_at + relativedelta(hours=res.count_time)
                elif res.dates == 5:
                    dates = res.start_at + relativedelta(minutes=res.count_time)
            elif res.plusminus == 2:
                if res.dates == 1:
                   dates = res.start_at - relativedelta(days=res.count_time)
                elif res.dates == 2:
                    dates = res.start_at - relativedelta(months=res.count_time)
                elif res.dates == 3:
                    dates = res.start_at - relativedelta(years=res.count_time)
                elif res.dates == 4:
                    dates = res.start_at - relativedelta(hours=res.count_time)
                elif res.dates == 5:
                    dates = res.start_at - relativedelta(minutes=res.count_time)

            if choice == 1:
                if res.condition == 1:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(Q(account__in=accs)&Q(user=sub.account)&Q(created=dates))]
                elif res.condition == 2:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(Q(account__in=accs)&Q(user=sub.account)&~Q(created=dates))]
                elif res.condition == 3:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(account__in=accs,user=sub.account, created__gt=dates)]
                elif res.condition == 4:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(account__in=accs,user=sub.account, created__lt=dates)]
                elif res.condition == 5:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(account__in=accs,user=sub.account, created__gte=dates)]
                elif res.condition == 6:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(account__in=accs,user=sub.account, created__lte=dates)]
            elif choice == 2:
                if res.condition == 1:
                    accs += [s.account.id for s in Entered.objects.filter(Q(user=sub.account)&Q(created=dates))]
                elif res.condition == 2:
                    accs += [s.account.id for s in Entered.objects.filter(Q(user=sub.account)&~Q(created=dates))]
                elif res.condition == 3:
                    accs += [s.account.id for s in Entered.objects.filter(user=sub.account, created__gt=dates)]
                elif res.condition == 4:
                    accs += [s.account.id for s in Entered.objects.filter(user=sub.account, created__lt=dates)]
                elif res.condition == 5:
                    accs += [s.account.id for s in Entered.objects.filter(user=sub.account, created__gte=dates)]
                elif res.condition == 6:
                    accs += [s.account.id for s in Entered.objects.filter(user=sub.account, created__lte=dates)]

            choice = res.choice

        elif hasattr(res, 'wifi'):
            if choice == 1:
                if res.condition == 1:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(Q(account__in=accs)&Q(user=sub.account)&Q(wifi=res.wifi))]
                elif res.condition == 2:
                    accs = []
                    accs += [s.account.id for s in Entered.objects.filter(Q(account__in=accs)&Q(user=sub.account)&~Q(wifi=res.wifi))]
            elif choice == 2:
                if res.condition == 1:
                    accs += [s.account.id for s in Entered.objects.filter(Q(user=sub.account)&Q(wifi=res.wifi))]
                elif res.condition == 2:
                    accs += [s.account.id for s in Entered.objects.filter(Q(user=sub.account)&~Q(wifi=res.wifi))]

            choice = res.choice

        elif hasattr(res, 'count'):
            acs = []
            if choice == 1:
                for ac in Account.objects.filter(id__in=accs, user=sub.account):
                    couns = Entered.objects.filter(account=ac).count()
                    if res.condition == 1 and couns == res.count:
                        acs.append(ac.id)
                    elif res.condition == 2 and couns != res.count:
                        acs.append(ac.id)
                    elif res.condition == 3 and couns > res.count:
                        acs.append(ac.id)
                    elif res.condition == 4 and couns < res.count:
                        acs.append(ac.id)
                    elif res.condition == 5 and couns >= res.count:
                        acs.append(ac.id)
                    elif res.condition == 6 and couns <= res.count:
                        acs.append(ac.id)
                accs = []
                accs += acs
            elif choice == 2:
                for ac in Account.objects.filter(user=sub.account):
                    couns = Entered.objects.filter(account=ac).count()
                    if res.condition == 1 and couns == res.count:
                        acs.append(ac.id)
                    elif res.condition == 2 and couns != res.count:
                        acs.append(ac.id)
                    elif res.condition == 3 and couns > res.count:
                        acs.append(ac.id)
                    elif res.condition == 4 and couns < res.count:
                        acs.append(ac.id)
                    elif res.condition == 5 and couns >= res.count:
                        acs.append(ac.id)
                    elif res.condition == 6 and couns <= res.count:
                        acs.append(ac.id)
                accs += acs
            choice = res.choice


      #print(choice)
      send_condition(sub.id, accs)