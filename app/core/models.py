# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
from accounts import UserProfile


class BaseModel(models.Model):
    is_active = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


"""
#
# Account
#
"""
class Account(BaseModel):
    user = models.ForeignKey(UserProfile, null=True, related_name='user_account')
    phone = models.CharField('phone', max_length=15)

"""
#
# PointWifi
#
"""
class PointWifi(BaseModel):
    user = models.ForeignKey(UserProfile, null=True, related_name='user_b_wifi')
    name = models.CharField('name', max_length=255)

"""
#
# Messages
#
"""


class Messages(BaseModel):
    STATUS_MES = (
    (0, 'Успешно доставлено!'),
    (1, 'You have exceeded the submission capacity allowed on this account, please back-off and retry'),
    (2, 'Your request is incomplete and missing some mandatory parameters'),
    (3, 'The value of one or more parameters is invalid'),
    (4, 'The api_key / api_secret you supplied is either invalid or disabled'),
    (5, 'An error has occurred in the Nexmo platform whilst processing this message'),
    (6, 'The Nexmo platform was unable to process this message, for example, an un-recognized number prefix or the '
        'number is not whitelisted if your account is new'),
    (7, 'The number you are trying to submit to is blacklisted and may not receive messages'),
    (8, 'The api_key you supplied is for an account that has been barred from submitting messages'),
    (9, 'Your pre-pay account does not have sufficient credit to process this message'),
    (11, 'This account is not provisioned for REST submission, you should use SMPP instead'),
    (12, 'Applies to Binary submissions, where the length of the UDH and the message body combined exceed 140 octets'),
    (13, 'Message was not submitted because there was a communication failure'),
    (14, 'Message was not submitted due to a verification failure in the submitted signature'),
    (15, 'The sender address (from parameter) was not allowed for this message. Restrictions may apply depending '
         'on the destination see ou'),
    (16, 'The ttl parameter values is invalid'),
    (19, 'Your request makes use of a facility that is not enabled on your account'),
    (20, 'The message class value supplied was out of range (0 - 3)'),
    )
    base_account = models.ForeignKey(UserProfile, null=True, related_name='user_messages')
    account = models.ForeignKey(Account, null=True, related_name='account_messages')
    wifi = models.ForeignKey(PointWifi, null=True, related_name='wifi_messages')
    text = models.CharField('Text', max_length=560, blank=True)
    admin = models.BooleanField(default=False)
    id_ms = models.CharField('ID', blank=True, null=True, max_length=250)

    status = models.IntegerField('Статус', default=0, choices=STATUS_MES)

"""
#
# Entered
#
"""
class Entered(BaseModel):
    user = models.ForeignKey(UserProfile, null=True, related_name='base_entered_messages')
    wifi = models.ForeignKey(PointWifi, null=True, related_name='wifi_entered')
    account = models.ForeignKey(Account, null=True, related_name='entered_messages')


"""
#
# Subscriptions (pattern)
#
"""
RUN_SUB_CHOICES = (
    (1, 'Не рассылать'),
    (2, 'Разослать 1 раз'),
    (3, 'Рассылка по условиям'),
)




class Subscriptions(models.Model):
   #condition = Условие расcылки
   account = models.ForeignKey(UserProfile, null=True, related_name='user_subs')

   title = models.CharField('Имя шаблона', max_length=255)
   content = models.CharField('Текст сообщения', max_length=560)
   phone = models.IntegerField('Подпись отправителя')

   run_sub = models.IntegerField('Выполнять рассылку периодически по фильтру',default=1, choices=RUN_SUB_CHOICES)

   now = models.BooleanField('Сейчас', default=False, blank=True)

   start_at = models.DateTimeField('Дата начала рассылки', blank=True, null=True)

   send = models.BooleanField('Отправлено', default=False, blank=True)

CONDITION_CHOICES = (
    (1, 'Равно'),
    (2, 'Не равно'),
    (3, 'Больше'),
    (4, 'Меньше'),
    (5, 'Больше или равно'),
    (6, 'Меньше или равно'),
    (7, 'Изменено'),
)

PLUSMINUS_CHOICES = (
    (1, '+'),
    (2, '-')
)

DATES_CHOICES = (
    (1, 'Дней'),
    (2, 'Месяцев'),
    (3, 'Лет'),
    (4, 'Часов'),
    (5, 'Минут'),
)

CHOICES = (
    (1, 'И'),
    (2, 'ИЛИ')
)


class Condition(models.Model):
    condition = models.IntegerField('Сравнение',default=1, choices=CONDITION_CHOICES)
    location = models.IntegerField(default=1, blank=True)
    choice = models.IntegerField('Выбор сравнения', default=None, null=True, blank=True, choices=CHOICES)

    class Meta:
        abstract = True

class ConditionTime(Condition):
    start_at = models.DateTimeField('Дата отчета', blank=True, null=True)
    plusminus = models.IntegerField('Плюс минус',default=1, choices=PLUSMINUS_CHOICES)
    count_time = models.IntegerField(default=0, blank=True)
    dates = models.IntegerField('Выбор дат',default=1, choices=DATES_CHOICES)

class ConditionPoint(Condition):
    wifi = models.ForeignKey(PointWifi, blank=True, null=True, related_name='condition__point__wifi')

class ConditionCount(Condition):
    count = models.IntegerField('Количеств посещений', default=0)

class ConditionGroups(models.Model):
    subscription = models.ForeignKey(Subscriptions, blank=False, null=True, related_name='groups__sub__condition')
    time = models.ManyToManyField(ConditionTime)
    point = models.ManyToManyField(ConditionPoint)
    count = models.ManyToManyField(ConditionCount)


