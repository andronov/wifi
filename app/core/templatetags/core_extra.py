from django import template
from core.models import Entered, Messages

register = template.Library()

@register.filter(name='last_wifi')
def last_wifi(value):
    return Entered.objects.filter(account_id=value).order_by('-created')[0].wifi.name

@register.filter(name='last_date')
def last_date(value):
    return Entered.objects.filter(account_id=value).order_by('-created')[0].created.strftime("%d.%m.%y")

@register.filter(name='last_time')
def last_time(value):
    return Entered.objects.filter(account_id=value).order_by('-created')[0].created.strftime("%H.%M")

@register.filter(name='last_count')
def last_count(value):
    return Entered.objects.filter(account_id=value).count()

@register.filter(name='last_message')
def last_message(value):
    ms = Messages.objects.filter(account__id=value).order_by('-created')
    if ms:
        return ms[0].text
    else:
        return ''