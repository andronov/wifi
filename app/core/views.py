# -*- coding: utf-8 -*-
import json
import datetime
from django.conf import settings
from django.core import serializers
from django.db.models import Count
from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response, redirect

# Create your views here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView
from django.contrib import messages
from nexmo.error_messages import (NEXMO_STATUSES, UNKNOWN_STATUS,
                             NEXMO_MESSAGES, UNKNOWN_MESSAGE)
from rest_framework.generics import RetrieveAPIView
from core.forms import SubForm
from core.models import *
from core.send import send
from core.serializers import MessagesSerializer, PointWifiSerializer, SubSerializer


class SubscriptionView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(SubscriptionView, self).get_context_data(**kwargs)

        if self.request.user.is_authenticated():
            enter, ent = [], []
            for en in Entered.objects.filter(user=self.request.user):
                if en.account.id not in enter:
                    enter.append(en.account.id)
                    ent.append(en.id)
            context['form'] = SubForm(initial={'phone': self.request.user.phone})
            context['subscr'] = Subscriptions.objects.filter(account=self.request.user)
            context['accounts'] = Account.objects.filter(user=self.request.user)
            context['entered'] = Entered.objects.filter(id__in=ent).order_by('-created')
            context['wifi'] = PointWifi.objects.filter(user=self.request.user)
        return context


def phone(request, id=None):
    print(id)
    ms = Messages.objects.filter(wifi__id=id)
    return serializers.serialize('json', ms)



class PointWifiPhoneDetail(RetrieveAPIView):
    model = PointWifi
    queryset = PointWifi.objects.all()
    serializer_class = PointWifiSerializer
    lookup_field = 'pk'

    def retrieve(self, request, *args, **kwargs):
        response = super(PointWifiPhoneDetail, self).retrieve(request, *args, **kwargs)
        msgs = Messages.objects.filter(wifi=self.get_object()).order_by('-created')
        ms_in = [d.account.id for d in Messages.objects.filter(wifi=self.get_object()).order_by('-created')]
        response.data = {
            'object': response.data,
            'messages': [MessagesSerializer(item).data for item in msgs],
        }
        return response

class ChatDetail(RetrieveAPIView):
    model = PointWifi
    queryset = PointWifi.objects.all()
    serializer_class = PointWifiSerializer
    lookup_field = 'pk'

    def retrieve(self, request, *args, **kwargs):
        response = super(ChatDetail, self).retrieve(request, *args, **kwargs)
        msgs = Messages.objects.filter(wifi=self.get_object(), account__id=self.request.GET['account']).order_by('-created')

        response.data = {
            'object': response.data,
            'messages': [MessagesSerializer(item).data for item in msgs],
        }
        return response

"""
#
# Subscriptions
#
"""
def sub_add(request):
    data = {}
    context = RequestContext(request)

    if request.POST:
        sub_forms = SubForm(request.POST)
        if sub_forms.is_valid():
            sub = sub_forms.save(commit=False)
            sub.account = request.user
            sub.save()
            #send(sub.id)
            if request.POST['run_sub'] == '3':
                #print(request.POST.items())
                condtion_time = []
                condtion_point = []
                condtion_count = []
                print(request.POST)
                for rei in request.POST.items():
                    if not rei[1]:
                        data['mistake'] = rei[0]
                        sub.delete()
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    else:
                        if rei[0].find('condition-time') != -1:
                            condtion_time.append(rei)
                        elif rei[0].find('condition-point') != -1:
                            condtion_point.append(rei)
                        elif rei[0].find('condition-count') != -1:
                            condtion_count.append(rei)

                condtpoint, condttime, condtcount = [], [], []

                if condtion_time:
                    condt = dict((x, y) for x, y in condtion_time)
                    i = 0
                    while i <= len(condt)/8:
                        try:
                            condtime = ConditionTime.objects.create(condition=condt['condition-time-'+str(i)+'-condition'],
                                                        location=condt['condition-time-'+str(i)+'-position'],
                                                        choice=condt['condition-time-'+str(i)+'-choice'],
                                                        start_at=datetime.datetime.now(),
                                                        plusminus=condt['condition-time-'+str(i)+'-plusminus'],
                                                        count_time=condt['condition-time-'+str(i)+'-count_time'],
                                                        dates=condt['condition-time-'+str(i)+'-dates'])
                            condttime.append(condtime.id)
                        except:
                            pass
                        i += 1

                if condtion_point:
                    condp = dict((x, y) for x, y in condtion_point)
                    i = 0
                    while i <= len(condp)/5:
                        try:
                            condpoint = ConditionPoint.objects.create(condition=condp['condition-point-'+str(i)+'-condition'],
                                                        location=condp['condition-point-'+str(i)+'-position'],
                                                        choice=condp['condition-point-'+str(i)+'-choice'],
                                                        wifi=PointWifi.objects.get(id=condp['condition-point-'+str(i)+'-wifi']))
                            condtpoint.append(condpoint.id)
                        except:
                            pass
                        i += 1

                if condtion_count:
                    condc = dict((x, y) for x, y in condtion_count)
                    print(len(condc))
                    i = 0
                    while i <= len(condc)/5:
                        try:
                            condcount = ConditionCount.objects.create(condition=condc['condition-count-'+str(i)+'-condition'],
                                                        location=condc['condition-count-'+str(i)+'-position'],
                                                        choice=condc['condition-count-'+str(i)+'-choice'],
                                                        count=condc['condition-count-'+str(i)+'-mcount'])
                            condtcount.append(condcount.id)
                        except:
                            pass
                        i += 1

                totalcond = ConditionGroups()
                totalcond.subscription = Subscriptions.objects.get(id=sub.id)
                totalcond.save()
                if condttime:
                    for t in ConditionTime.objects.filter(id__in=condttime):
                        totalcond.time.add(t)
                if condtpoint:
                    for p in ConditionPoint.objects.filter(id__in=condtpoint):
                        totalcond.point.add(p)
                if condtcount:
                    for c in ConditionCount.objects.filter(id__in=condtcount):
                        totalcond.count.add(c)
                totalcond.save()





            context['sub_forms'] = sub_forms
            context['sub'] = sub
            data['id'] = sub.id
            data['html'] = render_to_string("settings/add.html", context)
        else:
            context['errors'] = True
            data['errors'] = True
            context['sub_forms'] = sub_forms
            #context['sub'] = sub

            data['html'] = render_to_string("settings/add.html", context)


        #return redirect('home')
        return HttpResponse(json.dumps(data), content_type="application/json")
    else:
        print('NO POST')
    return render_to_response("settings/add.html", context_instance=context)


def sub_edit(request, id):
    data = {}
    context = RequestContext(request)
    sub = Subscriptions.objects.get(id=id)

    res = []

    if sub.run_sub == 3:
       sub_groups = ConditionGroups.objects.get(subscription=sub)

       order = {}
       #count
       for cnt in sub_groups.count.all():
          order[cnt.location] = cnt

       #point
       for cnt in sub_groups.point.all():
          order[cnt.location] = cnt

       #time
       for cnt in sub_groups.time.all():
          order[cnt.location] = cnt

       for ord in order:
           res.append(order[ord])

    enter, ent = [], []
    for en in Entered.objects.filter(user=request.user):
        if en.account.id not in enter:
            enter.append(en.account.id)
            ent.append(en.id)

    if request.POST:
        sub_forms = SubForm(request.POST, instance=sub)
        if sub_forms.is_valid():
            sub = sub_forms.save(commit=False)
            sub.account = request.user
            sub.save()

            if request.POST['run_sub'] == '2':
                if 'now'in request.POST and sub.send == False:
                   send(sub.id)
                   sub.send = True
                   sub.save()
            elif request.POST['run_sub'] == '3':
                #print(request.POST.items())
                condtion_time = []
                condtion_point = []
                condtion_count = []
                print(request.POST)
                for rei in request.POST.items():
                    if not rei[1]:
                        data['mistake'] = rei[0]
                        sub.delete()
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    else:
                        if rei[0].find('condition-time') != -1:
                            condtion_time.append(rei)
                        elif rei[0].find('condition-point') != -1:
                            condtion_point.append(rei)
                        elif rei[0].find('condition-count') != -1:
                            condtion_count.append(rei)

                condtpoint, condttime, condtcount = [], [], []

                if condtion_time:
                    condt = dict((x, y) for x, y in condtion_time)
                    i = 0
                    while i <= len(condt)/8:
                        try:
                             condtime = ConditionTime.objects.create(condition=condt['condition-time-'+str(i)+'-condition'],
                                                        location=condt['condition-time-'+str(i)+'-position'],
                                                        choice=condt['condition-time-'+str(i)+'-choice'],
                                                        start_at=datetime.datetime.now(),
                                                        plusminus=condt['condition-time-'+str(i)+'-plusminus'],
                                                        count_time=condt['condition-time-'+str(i)+'-count_time'],
                                                        dates=condt['condition-time-'+str(i)+'-dates'])
                             condttime.append(condtime.id)
                        except:
                            pass
                        i += 1

                if condtion_point:
                    condp = dict((x, y) for x, y in condtion_point)
                    i = 0
                    while i <= len(condp)/5:
                        try:
                            condpoint = ConditionPoint.objects.create(condition=condp['condition-point-'+str(i)+'-condition'],
                                                        location=condp['condition-point-'+str(i)+'-position'],
                                                        choice=condp['condition-point-'+str(i)+'-choice'],
                                                        wifi=PointWifi.objects.get(id=condp['condition-point-'+str(i)+'-wifi']))
                            condtpoint.append(condpoint.id)
                        except:
                            pass
                        i += 1

                if condtion_count:
                    condc = dict((x, y) for x, y in condtion_count)
                    print(len(condc))
                    i = 0
                    while i <= len(condc)/5:
                        try:
                           condcount = ConditionCount.objects.create(condition=condc['condition-count-'+str(i)+'-condition'],
                                                        location=condc['condition-count-'+str(i)+'-position'],
                                                        choice=condc['condition-count-'+str(i)+'-choice'],
                                                        count=condc['condition-count-'+str(i)+'-mcount'])
                           condtcount.append(condcount.id)
                        except:
                            pass
                        i += 1
                ConditionGroups.objects.get(subscription=sub).delete()
                totalcond = ConditionGroups()
                totalcond.subscription = Subscriptions.objects.get(id=sub.id)
                totalcond.save()
                if condttime:
                    for t in ConditionTime.objects.filter(id__in=condttime):
                        totalcond.time.add(t)
                if condtpoint:
                    for p in ConditionPoint.objects.filter(id__in=condtpoint):
                        totalcond.point.add(p)
                if condtcount:
                    for c in ConditionCount.objects.filter(id__in=condtcount):
                        totalcond.count.add(c)
                totalcond.save()





            context['sub_forms'] = sub_forms
            context['sub'] = sub
            data['id'] = sub.id
            data['html'] = render_to_string("settings/add.html", context)
        else:
            context['errors'] = True
            data['errors'] = True
            context['sub_forms'] = sub_forms
            #context['sub'] = sub

            data['html'] = render_to_string("settings/add.html", context)

    sub_forms = SubForm(instance=sub)
    subscr = Subscriptions.objects.filter(account=request.user)
    accounts = Account.objects.filter(user=request.user)
    entered = Entered.objects.filter(id__in=ent).order_by('-created')
    wifi = PointWifi.objects.filter(user=request.user)

    #return HttpResponse(json.dumps(data), content_type="application/json")
    return render_to_response('home.html', {"form": sub_forms, 'subscr': subscr, 'wifi': wifi, 'sub': sub,
                                            'accounts': accounts, 'entered': entered, 'orders': res},
                              context_instance=context)

def sub_delete(request, id=''):
    print(id, 'delete')
    sub = Subscriptions.objects.get(id=id)
    sub.delete()

    context = RequestContext(request)

    return render_to_response('settings/delete.html', context_instance=context)

def sub_copy(request, id=''):
    print(id, 'copy')
    subs = Subscriptions.objects.get(id=id)
    if subs.run_sub == 3:
        subs.pk = None
        subs.save()
        grp = ConditionGroups.objects.get(subscription__id=id)

        grp_count, grp_point, grp_time = [], [], []

        for cnt in grp.count.all():
            cnt.pk = None
            cnt.save()
            grp_count.append(cnt.id)

        for cnt in grp.point.all():
            cnt.pk = None
            cnt.save()
            grp_point.append(cnt.id)

        for cnt in grp.time.all():
            cnt.pk = None
            cnt.save()
            grp_time.append(cnt.id)


        grp.pk = None
        grp.subscription = subs
        grp.save()
        if grp_time:
            for t in ConditionTime.objects.filter(id__in=grp_time):
                grp.time.add(t)
        if grp_point:
            for p in ConditionPoint.objects.filter(id__in=grp_point):
                grp.point.add(p)
        if grp_count:
            for c in ConditionCount.objects.filter(id__in=grp_count):
                grp.count.add(c)
        grp.save()
    else:
        subs.pk = None
        subs.save()

    res = SubSerializer(subs).data
    return HttpResponse(json.dumps(res), content_type="application/json")
"""
#
# ADD MESSAGES
#
"""
def add_messages(request, wifi, id):
    wi = PointWifi.objects.get(id=wifi)
    i = Account.objects.get(id=id)
    if request.POST['message']:
        ms = Messages.objects.create(base_account=request.user, account=i,
                                     wifi=wi, text=request.POST['message'], admin=True)
        res = MessagesSerializer(ms).data
        from nexmo.libpynexmo.nexmomessage import NexmoMessage
        params = {
                'api_key': settings.NEXMO_USERNAME,
                'api_secret': settings.NEXMO_PASSWORD,
                'type': 'unicode',
                'from': '7' +str(request.user.phone),
                'to': '+7' +str(i.phone),
                'text': request.POST['message'].encode('utf-8'),
        }
        sms = NexmoMessage(params)
        response = sms.send_request()

        msa = Messages.objects.get(id=ms.id)
        msa.status = response['messages'][0]['status']
        msa.save()

        if int(response['messages'][0]['status']) == 0:
            msa.id_ms = response['messages'][0]['message-id']
            msa.save()
        return HttpResponse(json.dumps(res), content_type="application/json")
    else:
        res = {'error': True}
        return HttpResponse(json.dumps(res), content_type="application/json")

"""
#
# RECEIVE MESSAGES
#
"""
def receive_ms(request):
    phone = request.GET['msisdn']
    text = request.GET['text']
    id = request.GET['messageId']
    acc = Account.objects.filter(phone=phone[1:]).order_by('-created')[0]
    try:
        entered = Entered.objects.filter(user=acc.user, account=acc).order_by('-created')[0]
    except:
        entered = Messages.objects.filter(base_account=acc.user, account=acc).order_by('-created')[0]
    ms = Messages.objects.create(base_account=acc.user, account=acc,
                                 wifi=entered.wifi, text=text, id_ms=id)
    """
    tmp_filename = settings.TMP_PATH + '/messages.txt'
    with open(tmp_filename, "wb") as f:
           f.write(str(request.GET))
    f.close()
    """
    return HttpResponse('')


class PopupView(TemplateView):
    template_name = 'form.html'