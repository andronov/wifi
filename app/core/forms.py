# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.forms import ModelForm
from django.forms.formsets import BaseFormSet, formset_factory


from bootstrap3.tests import TestForm
from core.models import Subscriptions

"""
RUN_SUB_CHOICES = (
    (1, 'Не рассылать'),
    (2, 'Разослать 1 раз'),
    (3, 'Рассылка по условиям'),
)



class SubForm(forms.Form):
    title = forms.CharField(label='Имя шаблона', max_length=100)
    content = forms.CharField(label='Текст сообщения', widget=forms.Textarea)
    phone = forms.IntegerField(label='Подпись отправителя')
    run_sub = forms.ChoiceField(label='Выполнять рассылку периодически по фильтру', choices=RUN_SUB_CHOICES)
    date = forms.DateField(label='Дата начала рассылки')

class ContactBaseFormSet(BaseFormSet):
    def add_fields(self, form, index):
        super(ContactBaseFormSet, self).add_fields(form, index)

    def clean(self):
        super(ContactBaseFormSet, self).clean()
        raise forms.ValidationError("This error was added to show the non form errors styling")

ContactFormSet = formset_factory(SubForm, formset=ContactBaseFormSet,
                                 extra=1,
                                 max_num=5,
                                 validate_max=True)

"""
class SubForm(ModelForm):
     class Meta:
         model = Subscriptions
         fields = ['title', 'content', 'phone', 'run_sub', 'start_at']