import datetime
from rest_framework import serializers
from core.models import Messages, PointWifi, Account, Subscriptions


class AccountSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = Account
        fields = ('pk', 'phone')

class PointWifiSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = PointWifi
        fields = ('pk', 'name')

class MessagesSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    account = AccountSerializer(many=False, read_only=True)
    datetime = serializers.SerializerMethodField('time')
    status = serializers.ChoiceField(choices=Messages.STATUS_MES)
    status_display = serializers.CharField(source='get_status_display',read_only=True)

    def time(self, obj):
        if datetime.datetime.now().strftime("%Y%m%d") == obj.created.strftime("%Y%m%d"):
            return obj.created.strftime("%H:%M")
        else:
            return obj.created.strftime("%Y.%m.%d %H:%M")

    class Meta:
        model = Messages
        fields = ('pk', 'base_account', 'account', 'status', 'status_display', 'status', 'id_ms', 'wifi',
                  'text','admin', 'created', 'datetime')

class SubSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    account = AccountSerializer(many=False, read_only=True)

    class Meta:
        model = Subscriptions
        fields = ('pk', 'account', 'title', 'content', 'phone', 'run_sub', 'now', 'start_at')