from django.contrib import admin

# Register your models here.
from core.models import *

admin.site.register(Account)
admin.site.register(PointWifi)
admin.site.register(Messages)
admin.site.register(Subscriptions)
admin.site.register(Entered)
admin.site.register(ConditionPoint)
admin.site.register(ConditionCount)
admin.site.register(ConditionTime)
admin.site.register(ConditionGroups)