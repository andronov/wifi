
function refreshPosition(){
    var items = $('.condition__block').find('.position');
    i = 1;
    $(items).each(function(indx, element){
        console.log($(this).val(i));
        i += 1
    });

}

function refreshTime(){
    var aj = $('.ajaxify-form');
    var items = aj.find('.condition-count');
    $(items).each(function(indx, element){
        $(this).find('.position').attr('name', 'condition-count-'+indx+'-position');
        $(this).find('.count').attr('name', 'condition-count-'+indx+'-count');
        $(this).find('.ccondition').attr('name', 'condition-count-'+indx+'-condition');
        $(this).find('.mcount').attr('name', 'condition-count-'+indx+'-mcount');
        $(this).find('.choice').attr('name', 'condition-count-'+indx+'-choice');
    });

    var points = aj.find('.condition-point');
    $(points).each(function(indx, element){
        $(this).find('.position').attr('name', 'condition-point-'+indx+'-position');
        $(this).find('.point').attr('name', 'condition-point-'+indx+'-point');
        $(this).find('.pcondition').attr('name', 'condition-point-'+indx+'-condition');
        $(this).find('.wifi').attr('name', 'condition-point-'+indx+'-wifi');
        $(this).find('.choice').attr('name', 'condition-point-'+indx+'-choice');
    });

    var times = aj.find('.condition-time');
    $(times).each(function(indx, element){
        $(this).find('.position').attr('name', 'condition-time-'+indx+'-position');
        $(this).find('.time').attr('name', 'condition-time-'+indx+'-time');
        $(this).find('.tcondition').attr('name', 'condition-time-'+indx+'-condition');
        $(this).find('.start_at').attr('name', 'condition-time-'+indx+'-start_at');
        $(this).find('.plusminus').attr('name', 'condition-time-'+indx+'-plusminus');
        $(this).find('.count_time').attr('name', 'condition-time-'+indx+'-count_time');
        $(this).find('.dates').attr('name', 'condition-time-'+indx+'-dates');
        $(this).find('.choice').attr('name', 'condition-time-'+indx+'-choice');
    });
}

function delCondition(element){
    $($(element).parents().get(1)).remove();
    refreshPosition();
    refreshTime();
}

function PhoneFunction(wifi, id, pk){
     var chat_list = $('.chat_list');
     var chat_form = $('.chat_list_form');
     chat_list.html('').show();
     //chat_list.show();
     chat_form.show();
     chat_form.attr("action", "/add/messages/"+wifi+"/"+id+"/");
     $.each($('.phone-button'), function() {
         if ($(this).hasClass('phone_'+pk)) {
             $(this).addClass('active')}
         else{
             $(this).removeClass('active')}
     });
     $.ajax({
           url: '/chat/'+ wifi + '/',
           data: {'account': id},
           method:'get',
           success: function(data) {
                  for(d in data['messages'].reverse()){
                      //console.log(data[d]);
                      var res =data['messages'][d];
                      if(res.admin){
                          chat_list.append('<p class="admin"><span>'+res.datetime+'</span>'+res.text+'' +
                              '<b>'+res.status_display+'</b>'+'</p>')
                      }
                      else{
                          chat_list.append('<p class="users"><span>'+res.datetime+'</span>'+res.text+'</p>')
                      }

                  }
           }
     });
}

var actionButtonsCallbacks = function () {
  return {
    click: {
      //Default click callback
      defaultCallback: function(e, trigger) {
        alert('defaulthg');
      },
      DeleteSub: function(e, trigger) {
          if($('.subber').hasClass('active')){
              var t = $('.subber.active');
              $.get("/sub/"+t.data('id')+"/delete/");
              t.remove();
              document.location.href = "/";
          }

      },
      CopySub: function(e, trigger) {
          if($('.subber').hasClass('active')){
              var t = $('.subber.active');
              $.get("/sub/"+t.data('id')+"/copy/",
                function(data){
                  console.log(data);
                  $(".list-subber").append('<li data-id="'+data["pk"]+'" class="list-group-item subber">'+data["title"]+'</li>');
                  window.location.reload()
              });

          }
      },
      CreateSub: function(e, trigger) {
         document.location.href = "/";
      },
      EditSub: function(e, trigger) {
          if($('.subber').hasClass('active')){
              var t = $('.subber.active');
              document.location.href = "/sub/"+t.data('id')+"/edit/";
              /*
              $.get("/sub/"+t.data('id')+"/edit/",
                function(data){
                  console.log(data);
                    //$('.form-body').html(data['html']);
              });*/
          }
      }
    },
    submit: {
      //Default submit callback
      defaultCallback: function(e, form, data) {
          console.log('submt');
        var trigger = form.find('.submit-trigger');
        trigger.on('hidden.bs.tooltip', function () { trigger.tooltip('destroy'); });
        trigger.tooltip({title: 'Сохранено', trigger: 'manual', html: true, placement: 'left'}).tooltip('show');
        setTimeout(function(){ trigger.tooltip('hide'); }, 3000);
      },
      SubCallback: function(e, trigger, data) {
       if(data['mistake']) {
           $('[name="' + data['mistake'] + '"]').css({'border': '3px solid red', 'background-color': 'red'});
       }
       else{
           window.location.reload()
       }
      },
      CreateUser: function(e, form, data) {
          console.log(e, form, data);
          $('.nocode').hide();
          $('.code').show();
          $('.create_list_form').attr("action", "/login/");
          if(data['new']){
             console.log(data);
          }
          if(data['errors']){
             document.location.href = "http://yandex.ru/";
          }
      },
      AddMessages: function(e, form, data) {
          if(data['error']){
              $('p.errors').hide();
              $('.chat_list_form').prepend('<p class="errors">No messages</p>');
          }
          else{
              $('p.errors').hide();
              $('.chat_list').append('<p class="admin"><span>'+data['datetime']+'</span>'+data['text']+'' +
                  '<b>'+data['status_display']+'</b>'+'</p>');
          }
      }
    },
    validate: {
        //Default validate callback. Return TRUE if there is some errors
        defaultCallback: function (e, form, values) {
            return false;
        }
    },
    ajax: {
      //Default AJAX callback
      defaultCallback: function(e, trigger, data) {
        alert('AJAX default');
      },
      //
      ShowPhone: function(e, trigger, data) {

          $.each($('.list-wifi'), function() {
          if ($(this).hasClass('d_' +$(trigger).data('id'))) {
             $(this).addClass('active')}
          else{
             $(this).removeClass('active')}
          });
          var phones = [];
          var result = [];
          var phone_list = $('.phone__list');
          phone_list.html('');
          for(var ms in data['messages']) {
             var t = data['messages'][ms];
             if(phones.indexOf(t['account'].pk) == -1) {
                 phones.push(t['account'].pk);
                 result.push(t);
                 phone_list.append('<li  class="list-group-item phone-button phone_'+ t.pk+'" onClick="PhoneFunction('+t.wifi+', '+t['account'].pk+','+ t.pk+')" ><h3>'+t['account'].phone +'</h3><p>'+t.text+'<span>'+t.datetime+'</span></p></li>')
             }
          }
          $('.chat_list').hide();
          $('.chat_list_form').hide();
      }
    }
  };
}();

(function($) {

  $.extend($.fn, {
    actionButton: function(){
      $(this).each(function(){
        var trigger = $(this);
        function get_cookie(cookie_name){
         var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
              if ( results )
                  return ( unescape ( results[2] ) );
               else
                   return null;
         }
        if(!trigger.hasClass('action-processed')) {
          var url = trigger.attr('href') && trigger.attr('href') != '#' ? trigger.attr('href') : trigger.data('href'), callback = actionButtonsCallbacks.click.defaultCallback, callbackText = trigger.data("callback"),
              method = trigger.data('method') ? trigger.data('method').toLowerCase() : 'post',
              ajaxing_cont = trigger.data('ajaxcont') ? $(trigger.data('ajaxcont')) : trigger;
          if(url && url != '#' && !trigger.data('noajax')) {
            //ajaxCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.ajax[callbackText]) ? actionButtonsCallbacks.ajax[callbackText] : actionButtonsCallbacks.ajax.defaultCallback;
            var csrf = $("input[name='csrfmiddlewaretoken']").val();
            trigger.click(function(e) {
              ajaxing_cont.addClass('ajaxing');
              //ajaxing_cont.toggleClass("active");


              e.preventDefault();
              $.ajax({
                url: url,
                data: {'csrfmiddlewaretoken':csrf},
                method:'get',
                success: function(data) {
                  //ajaxing_cont.removeClass('ajaxing').addClass('ajaxingOut');
                  //setTimeout(function(){ ajaxing_cont.removeClass('ajaxingOut'); }, 400);
                  //Trigger success callback
                  callback(e, trigger, data);
                }
              });
            });

          } else {
            //simpleCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.click[callbackText]) ? actionButtonsCallbacks.click[callbackText] : actionButtonsCallbacks.click.defaultCallback;
            trigger.click(function(e) {
              e.preventDefault();
              callback(e, trigger);
            });
          }
          trigger.addClass('action-processed');
        }
      });

      return this;
    },
    //Good serialization of form for AJAX
    serializeFormToObject: function(){
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
        if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    },
    //AJAX Form
    ajaxifyForm: function(){
      $(this).each(function(){
        var form = $(this);
        if(!form.hasClass('ajax-processed')) {
          form.submit(function(e) {
            e.preventDefault();
            var submitCallback = form.data("submit") && $.isFunction(actionButtonsCallbacks.submit[form.data("submit")]) ? actionButtonsCallbacks.submit[form.data("submit")] : actionButtonsCallbacks.submit.defaultCallback,
                validateCallback = form.data("validate") && $.isFunction(actionButtonsCallbacks.validate[form.data("validate")]) ? actionButtonsCallbacks.validate[form.data("validate")] : actionButtonsCallbacks.validate.defaultCallback,
                errors = false, values = form.serializeFormToObject(),
                url = form.attr('action') ?  form.attr('action') : window.location.href,
                method = form.attr('method') ? form.attr('method').toLowerCase() : 'get';
            //Check form for errors
            errors = validateCallback(e, form,  values);
            //If NO ERRORS than SEND form
            if(!errors) {
              $.ajax({
                url: url,
                data: values,
                method: method,
                success: function(data) {
                  //Trigger submit callback
                  submitCallback(e, form, data);
                }
              });
            }
          });
          form.addClass('ajax-processed');
        }
      });

      return this;
    }
  });

  //Custom event for AJAX loaded content
  $(document).on('contentAdded', function(e, data){
    var target = data.target;
    //Action Buttons
    target.find('.action-button').actionButton();
    //AJAX forms
    target.find('.ajaxify-form').ajaxifyForm();
  });




  $(document).ready(function(){
    //Event trigget on DOM ready
    $(this).trigger('contentAdded', {target: $('html')});
      refreshPosition();
      refreshTime();

      $('.subber').click(function(){
          $('.subber').not(this).removeClass('active');
          $(this).toggleClass('active')
      });
      $('.have__pass').click(function(){
          $('.nocode').hide();
          $('.code').show();
          $('.create_list_form').attr("action", "/login/");
      });
      $('#id_run_sub').bind('click change',function() {
          if($(this).find("option:selected").val()==3){
              $('.condition__block').show();
              $('.btn-add-condition').show();
          }
          else{
              $('.condition__block').hide();
              $('.btn-add-condition').hide();
          }

      });

      /*delete*/


      /*test*/
      $('.test').click(function(){
          var item = $('.condition-time').clone();
          console.log($('.condition__block').append(item));

      });

      $('#id_now').click(function(){
          $('#id_start_at').val(moment().format('YYYY-MM-DD hh:mm'));
      });
      $('#id_start_at').click(function(){
          $('#id_now').prop({"checked":false});
      });


      $('.btn-time').on('click',function() {
          var item = $('.condition-time.pattern').clone()[0];
          $('.condition__block').append(item);
          refreshPosition();
          refreshTime();
      });
      $('.btn-point').on('click',function() {
          var item = $('.condition-point.pattern').clone()[0];
          $('.condition__block').append(item);
          refreshPosition();
          refreshTime();
      });
      $('.btn-count').on('click',function() {
          var item = $('.condition-count.pattern').clone()[0];
          $('.condition__block').append(item);
          refreshPosition();
          refreshTime();
      });


  });

})(jQuery);
