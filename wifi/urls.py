"""wifi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from accounts.views import *
from core.utils import calc
from core.views import *
from django.contrib.staticfiles import views




urlpatterns = [
    url(r'^$', SubscriptionView.as_view(), name='home'),
    #url(r'^phone/(?P<id>[-\w]+)/$', phone, name='phone'),
    url(r'^phone/(?P<pk>[-\w]+)/', PointWifiPhoneDetail.as_view(), name='phone'),
    url(r'^chat/(?P<pk>[-\w]+)/', ChatDetail.as_view(), name='chat'),
    url(r'^sub/add/', sub_add, name='sub_add'),
    url(r'^sub/(?P<id>.*)/delete/$', sub_delete, name='sub_delete'),
    url(r'^sub/(?P<id>.*)/edit/$', sub_edit, name='sub_edit'),
    url(r'^sub/(?P<id>.*)/copy/$', sub_copy, name='sub_copy'),

    url(r'^add/messages/(?P<wifi>.*)/(?P<id>.*)/$', add_messages, name='add_messages'),

    url(r'^create/user/', create_account, name='create_account'),
    url(r'^login/', my_login, name='my_login'),

    url(r'^get/messages', receive_ms, name='receive_ms'),

    url(r'^test', calc, name='test'),

    url(r'^forms', PopupView.as_view(), name='forms'),

    url(r'^nexmo/', include('nexmo.urls')),

    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
]